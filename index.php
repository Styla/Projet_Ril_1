<?php
    date_default_timezone_set("Europe/Paris");
    require_once "./functions/classeAutoLoader.php";
    spl_autoload_register('classeAutoLoader');
?>
<!DOCTYPE html>
<html>
<head>

</head>
<body>
<?php
include "./includes/header.php";
include "./includes/main.php";
include "./includes/footer.php";
?>
</body>
</html>

