<?php
//Opérateur null coalescent
$page = $_GET['page'] ?? "home";
$page = "./includes/" . $page . ".inc.php";

$directorContent = glob('./includes/*.inc.php');

if(in_array($page, $directorContent)) {
    include $page;
}
else {
    include "./includes/home.inc.php";
}
