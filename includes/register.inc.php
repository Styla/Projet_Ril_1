<?php
if(isset($_POST["formRegister"])) {
    $name= $_POST["name"] ?? null;
    $firstName = $_POST["firstName"] ?? null;
    $mail = $_POST["mail"] ?? null;
    $password = $_POST["password"] ?? null;

    $erreurs = array();

    if ($name == "")        array_push($erreurs, "Veuillez saisir votre nom");
    if ($firstName == "")   array_push($erreurs, "Veuillez saisir votre prénom");
    if ($mail == "")        array_push($erreurs, "Veuillez saisir votre mail");
    if ($password == "")    array_push($erreurs, "Veuillez saisir un mot de passe");

    if (count($erreurs) > 0) {
        $message = "<ul>";

        for( $i = 0; $i < count($erreurs) ; $i++) {
            $message .= "<li>";
            $message .= $erreurs[$i];
            $message .= "</li>";
        }

        $message .= "</ul>";

        echo $message;

        include "./includes/formRegister.php";
    }
    else {
        $insertion = new Query();
        if ($insertion->insertUser($name, $firstName, $mail, $password))  echo "Erreur lors de l'ajout d'un user";
    }
}
else {
    include "./includes/formRegister.php";
}
