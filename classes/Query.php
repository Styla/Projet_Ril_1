<?php


class Query
{
    private $dsn = "mysql:dbname=blog;host=localhost;charset=utf8";
    private $user = "blog";
    private $password = "blog";
    private $db;

    public function __construct()
    {
        try {
            $this->db = new PDO($this-> dsn, $this->user, $this->password);
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        }
        catch (PDOException $e) {
            Log::logWrite($e->getMessage());
        }
    }

    public function insertUser($name, $firstName, $mail, $pw) {

        $pw = hash("sha256", $pw);
        $role = "2";
        $insertion =$this->db->prepare("INSERT INTO t_users (USENAME, USEFIRSTNAME, USEMAIL, USEPW, T_ROLES_ID_ROLE) VALUES (:name, :firstName, :userMail, :userPassword, :userRole)");
        $insertion->bindParam(':name', $name);
        $insertion->bindParam(':firstName', $firstName);
        $insertion->bindParam(':userMail', $mail);
        $insertion->bindParam(':userPassword', $pw);
        $insertion->bindParam(':userRole', $role);


        $userName = $name;
        $userFirstName = $firstName;
        $userMail = $mail;
        $userPassword = $pw;
        $userRole = '2';
        $insertion->execute();
    }

    public function __destruct()
    {
        unset($this->db);
    }
}